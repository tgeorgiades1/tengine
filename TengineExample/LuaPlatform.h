#pragma once
#include "LuaEntity.h"

class LuaPlatform : public LuaEntity {
public:
	LuaPlatform(const std::string& luaScript, const std::string& entityName);
	~LuaPlatform();

	void update();
};

