#include "LuaPlayer.h"
#include <SDL/SDL.h>

LuaPlayer::LuaPlayer(const std::string& luaScript, const std::string& entityName, tengine::InputManager* inputManager) : LuaEntity(luaScript, entityName) {
	_inputManager = inputManager;
}

LuaPlayer::~LuaPlayer() { }

void LuaPlayer::update() {
	handleInput();
	_script->callFunction("update");
	LuaEntity::setPosition(_script->get<float>("entity." + _entityName + ".position.x"), _script->get<float>("entity." + _entityName + ".position.y"));
}

void LuaPlayer::handleInput() {
	if(_inputManager->isKeyDown(SDLK_w)) {
		_script->set<int>("entity." + _entityName + ".dirVec.verticalDir", 1);
	}
	else if(_inputManager->isKeyDown(SDLK_s)) {
		_script->set<int>("entity." + _entityName + ".dirVec.verticalDir", -1);
	}
	else {
		_script->set<int>("entity." + _entityName + ".dirVec.verticalDir", 0);
	}

	if(_inputManager->isKeyDown(SDLK_a)) {
		_script->set<int>("entity." + _entityName + ".dirVec.horizDir", -1);
	}
	else if(_inputManager->isKeyDown(SDLK_d)) {
		_script->set<int>("entity." + _entityName + ".dirVec.horizDir", 1);
	}
	else {
		_script->set<int>("entity." + _entityName + ".dirVec.horizDir", 0);
	}

	if(_inputManager->isKeyPressed(SDLK_1)){
		_script->callFunction("sayHi");
	}
}