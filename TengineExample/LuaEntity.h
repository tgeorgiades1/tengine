#pragma once
#include <tengine/LuaScript.h>
#include <tengine/Sprite.h>
#include <string>

#include "Collidable.h"

class LuaEntity : public tengine::Sprite, public Collidable {
public:
	LuaEntity(const std::string& scriptPath, const std::string& entityName);
	virtual ~LuaEntity();

	void checkCollision(Collidable* c);

	virtual void update() = 0;

protected:
	tengine::LuaScript* _script;
	std::string _entityName;

	void setPosition(float x, float y) {
		Sprite::setPosition(x, y);
		_script->set<float>("entity." + _entityName + ".position.x", x);
		_script->set<float>("entity." + _entityName + ".position.y", y);
		Collidable::setPosition(glm::vec2(x, y));
	}
};
