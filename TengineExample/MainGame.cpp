#include "MainGame.h"
#include <tengine/Tengine.h>
#include <iostream>

MainGame::MainGame() : _running(true), _time(0.0f) { }

MainGame::~MainGame() { }

void MainGame::run() {
	initSystems();

	_running = true;
	gameLoop();
}

void MainGame::end() {
	std::cout << "Cleaning up...\n";
	for(auto s : _sprites) {
		delete s;
	}
	_window.destroy();
	tengine::cleanUp();
}

void testFunction() {
	std::cout << "This was called from a function pointer!\n";
}

void MainGame::initSystems() {
	std::cout << "Starting Tengine...\n";
	tengine::init();

	_time = 0;
	
	int _screenWidth = 800;
	int _screenHeight = 600;

	_window.create("Example", _screenWidth, _screenHeight, 0);
	_fpsTimer.setMaxFPS(60.0f);

	_camera = new tengine::Camera2D(_screenWidth, _screenHeight);

	// Call after window creation b/c that is where we initialize GLEW.
	initShaders();

	_spriteBatch.init();

	_hudContainer = new tengine::HUDContainer(_screenWidth, _screenHeight);

	_hudContainer->createButton(0, 0, "assets/art/GUI/Button.png", _inputManager, "assets/scripts/hi.lua");

	for(int i = 0; i < 1024; i += 64) {
		Platform* p = new Platform(i, 0, "assets/art/OpenGameArt/AlucardOGA/PlatformTiles/grass_main_64x64.png");
		p->attachShader(&_worldShader);
		_collidables.push_back(p);
		_platforms.push_back(p);
		_sprites.push_back(p);
	}

	for(int i = 0; i < 1024; i += 64) {
		for(int j = -64; j > -512; j -= 64){
			Platform* p = new Platform(i, j, "assets/art/OpenGameArt/AlucardOGA/PlatformTiles/dirt_64x64.png");
			p->attachShader(&_worldShader);
			_collidables.push_back(p);
			_platforms.push_back(p);
			_sprites.push_back(p);
		}
	}

	Platform* p = new Platform(500, 128, "assets/art/OpenGameArt/Amir027OGA/BlueBlock.png");
	p->attachShader(&_worldShader);
	_collidables.push_back(p);
	_platforms.push_back(p);
	_sprites.push_back(p);
	Platform* p1 = new Platform(700, 204, "assets/art/OpenGameArt/Amir027OGA/BlueBlock.png");
	p1->attachShader(&_worldShader);
	_collidables.push_back(p1);
	_platforms.push_back(p1);
	_sprites.push_back(p1);
	Platform* p2 = new Platform(600, 250, "assets/art/OpenGameArt/Amir027OGA/BrownBlock.png");
	p2->attachShader(&_worldShader);
	_collidables.push_back(p2);
	_platforms.push_back(p2);
	_sprites.push_back(p2);

	_player = new Player(300, 64, "assets/art/player.png", &_inputManager);
	_player->attachShader(&_worldShader);
	_sprites.push_back(_player);
}

void MainGame::initShaders() {
	_worldShader.compilePrograms("shaders/spriteShader.vert", "shaders/spriteShader.frag");
	_worldShader.linkShaders();
}

void MainGame::gameLoop() {
	// Main game loop
	while(_running) {
		_fpsTimer.start();

		_inputManager.update();

		processInput();

		_hudContainer->update();
		
		_player->update();

		for(auto collidable : _collidables) {
			_player->checkCollision(collidable);
		}

		// Center the camera over the player.
		_camera->setPosition(glm::vec2(_player->Sprite::getPosition().x + _player->Sprite::getWidth() / 2, _player->Sprite::getPosition().y + _player->Sprite::getHeight() / 2));

		_camera->update();

		drawGame();

		_FPS = _fpsTimer.end();

		// Print FPS every 50 frames.
		static int frameCounter = 0;
		frameCounter++;
		if(frameCounter == 50) {
			printf("FPS: %.2f\n", _FPS);
			frameCounter = 0;
		}
	}

	end();
}

void MainGame::processInput() {
	SDL_Event event;
	while(SDL_PollEvent(&event)) {
		switch(event.type) {
		case SDL_KEYDOWN:
			_inputManager.keyPressed(event.key.keysym.sym);
			break;
		case SDL_KEYUP:
			_inputManager.keyReleased(event.key.keysym.sym);
			break;
		case SDL_MOUSEBUTTONDOWN:
			_inputManager.keyPressed(event.button.button);
			break;
		case SDL_MOUSEBUTTONUP:
			_inputManager.keyReleased(event.button.button);
			break;
		case SDL_MOUSEMOTION:
			_inputManager.setMousePosition(event.motion.x, event.motion.y);
			break;
		case SDL_QUIT:
			_running = false;
			break;
		}
	}
}

void MainGame::drawGame() {
	glClearDepth(1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glActiveTexture(GL_TEXTURE0);

	// Draw everything.
	_spriteBatch.begin(tengine::SortType::TEXTURE);

	// Update shader data.
	_worldShader.uniform1i("textureSampler", 0);
	_worldShader.uniformMat4f("pMatrix", _camera->getWorldMatrix());
	_worldShader.uniform2f("lightPosition", _camera->convertScreenToWorld(_inputManager.getMousePosition()).x, _camera->convertScreenToWorld(_inputManager.getMousePosition()).y);
	
	for(auto sprite : _sprites) {
		sprite->draw(_spriteBatch);
	}

	_hudContainer->draw(_spriteBatch);

	_spriteBatch.end();

	_window.swapBuffer();
}