#include "Collidable.h"

Collidable::Collidable() {
	_collider = new tengine::Collider();
}

Collidable::Collidable(float x, float y, float width, float height) {
	_collider = new tengine::Collider(x, y, width, height);
}

Collidable::~Collidable() {
	delete _collider;
}