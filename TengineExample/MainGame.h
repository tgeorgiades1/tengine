#pragma once
#include <SDL/SDL.h>
#include <tengine/Window.h>
#include <tengine/ShaderProgram.h>
#include <tengine/Timing.h>
#include <tengine/InputManager.h>
#include <tengine/Camera2D.h>
#include <tengine/Sprite.h>
#include <tengine/SpriteBatch.h>
#include <tengine/HUDContainer.h>

#include "Platform.h"
#include "Player.h"

class MainGame {
public:
	MainGame();
	~MainGame();

	/// Start the game.
	void run();

	/// Ends the game and cleans everything up.
	void end();

private:
	bool _running;

	tengine::Window _window;

	Player* _player;
	tengine::HUDContainer* _hudContainer;

	std::vector<tengine::Sprite*> _sprites;
	std::vector<Collidable*> _collidables;
	std::vector<Platform*> _platforms;

	tengine::SpriteBatch _spriteBatch;
	tengine::Camera2D* _camera;

	tengine::ShaderProgram _worldShader;

	float _FPS;
	tengine::FPSTimer _fpsTimer;

	tengine::InputManager _inputManager;

	float _time;

	/// Initializes tengine.
	void initSystems();

	/// Initializes shaders.
	void initShaders();

	/// Main game loop.
	void gameLoop();

	void drawGame();

	void processInput();
};
