#include "LuaEntity.h"

LuaEntity::LuaEntity(const std::string& scriptPath, const std::string& entityName) {
	_script = new tengine::LuaScript(scriptPath);
	_entityName = entityName;
	Sprite::setPosition(_script->get<float>("entity." + _entityName + ".position.x"), _script->get<float>("entity." + _entityName + ".position.y"));
	Sprite::setTexture(_script->get<std::string>("entity." + _entityName + ".texturePath"));

	Collidable::setPosition(glm::vec2(Sprite::getPosition().x, Sprite::getPosition().y));
	Collidable::setDimensions(glm::vec2(Sprite::getWidth(), Sprite::getHeight()));
}

LuaEntity::~LuaEntity() {
	delete _script;
}

void LuaEntity::checkCollision(Collidable* c) {
	if(_collider->checkCollision(c->_collider)) {
		// The minimum distance possible between the two CENTER POINTS of the colliding sprites.
		const float MIN_DISTANCE_X = _collider->getWidth() / 2.0f + c->getWidth() / 2.0f;
		const float MIN_DISTANCE_Y = _collider->getWidth() / 2.0f + c->getHeight() / 2.0f;

		glm::vec2 dist = _collider->getCenter() - c->getCenter();

		float xDepth = MIN_DISTANCE_X - abs(dist.x);
		float yDepth = MIN_DISTANCE_Y - abs(dist.y);

		// Push out by the smaller depth.
		if(xDepth < yDepth) {
			if(dist.x > 0) {
				setPosition(Sprite::getPosition().x + xDepth, Sprite::getPosition().y);
			}
			else {
				setPosition(Sprite::getPosition().x - xDepth, Sprite::getPosition().y);
			}
		}
		else {
			if(dist.y > 0) {
				setPosition(Sprite::getPosition().x, Sprite::getPosition().y + yDepth);
			}
			else {
				setPosition(Sprite::getPosition().x, Sprite::getPosition().y - yDepth);
			}
		}
	}
}