#pragma once
#include <tengine/Sprite.h>
#include "Collidable.h"

class Platform : public tengine::Sprite, public Collidable {
public:
	float originalY, speed;

	Platform(float x, float y, const std::string& texturePath);
	~Platform();

	void update();

	void setPosition(float x, float y) {
		Sprite::setPosition(x, y);
		Collidable::setPosition(Sprite::getPosition());
	}
};
