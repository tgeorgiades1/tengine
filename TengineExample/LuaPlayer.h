#pragma once
#include <tengine/Sprite.h>
#include <tengine/LuaScript.h>
#include <tengine/InputManager.h>
#include "LuaEntity.h"

class LuaPlayer : public LuaEntity {
public:
	LuaPlayer(const std::string& luaScript, const std::string& entityName, tengine::InputManager* inputManager);
	~LuaPlayer();

	void update();

private:
	tengine::InputManager* _inputManager;

	void handleInput();
};
