entity = {
	player = {
		position = {x = 100, y = 110},
		texturePath = "assets/art/lizardFace.png",
		dirVec = {verticalDir = 0, horizDir = 0},
		speed = 10
	}
}

function update() 
	entity.player.position.x = entity.player.position.x + (entity.player.speed * entity.player.dirVec.horizDir)
	entity.player.position.y = entity.player.position.y + (entity.player.speed * entity.player.dirVec.verticalDir)
end

function sayHi()
	print("HI")
end