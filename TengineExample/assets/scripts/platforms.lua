entity = {
	platform1 = {
		texturePath = "assets/art/OpenGameArt/AlucardOGA/PlatformTiles/grass_main_64x64.png",
		position = {x = 0, y = 0}
	},

	platform2 = {
		texturePath = "assets/art/OpenGameArt/AlucardOGA/PlatformTiles/grass_main_64x64.png",
		position = {x = 64, y = 0}
	},

	platform3 = {
		texturePath = "assets/art/OpenGameArt/AlucardOGA/PlatformTiles/grass_main_64x64.png",
		position = {x = 128, y = 0}
	},

	platform4 = {
		texturePath = "assets/art/redSquare.png",
		position = {x = -64, y = 0}
	},

	platform5 = {
		texturePath = "assets/art/OpenGameArt/AlucardOGA/PlatformTiles/grass_main_64x64.png",
		position = {x = -128, y = 200}
	}
}