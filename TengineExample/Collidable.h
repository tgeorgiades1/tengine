#pragma once
#include <tengine/Sprite.h>
#include <tengine/Collider.h>

class Collidable {
public:
	Collidable();
	Collidable(float x, float y, float width, float height);
	~Collidable();

	void setPosition(glm::vec2 newPosition) { _collider->setPosition(newPosition); }
	void setDimensions(glm::vec2 dimensions) { _collider->setDimensions(dimensions); }

	glm::vec2 getPosition() { return _collider->getPosition(); }
	glm::vec2 getCenter() { return _collider->getCenter(); }

	float getWidth() { return _collider->getWidth(); }
	float getHeight() { return _collider->getHeight(); }

	tengine::Collider* _collider;
};
