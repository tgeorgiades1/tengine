#pragma once
#include <tengine/InputManager.h>
#include <tengine/Sprite.h>
#include "Collidable.h"

class Player : public tengine::Sprite, public Collidable {
public:
	Player(float x, float y, const std::string& texturePath, tengine::InputManager* inputManager);
	~Player();

	void update();
	void checkCollision(Collidable* c);

	void setPosition(float x, float y) {
		Sprite::setPosition(x, y);
		Collidable::setPosition(glm::vec2(x, y));
	}

private:
	const float SPEED = 10.0f;
	tengine::InputManager* _inputManager;

	void handleInput();
};
