#include "Player.h"
#include <SDL/SDL.h>

Player::Player(float x, float y, const std::string& texturePath, tengine::InputManager* inputManager) :
Sprite(x, y, texturePath),
Collidable(x, y, Sprite::getWidth(), Sprite::getHeight()) {
	_inputManager = inputManager;
}

Player::~Player() { }

void Player::update() {
	handleInput();
}

void Player::checkCollision(Collidable* c) {
	if(_collider->checkCollision(c->_collider)) {
		// The minimum distance possible between the two CENTER POINTS of the colliding sprites.
		const float MIN_DISTANCE_X = _collider->getWidth() / 2.0f + c->getWidth() / 2.0f;
		const float MIN_DISTANCE_Y = _collider->getWidth() / 2.0f + c->getHeight() / 2.0f;

		glm::vec2 dist = _collider->getCenter() - c->getCenter();

		float xDepth = MIN_DISTANCE_X - abs(dist.x);
		float yDepth = MIN_DISTANCE_Y - abs(dist.y);

		// Push out by the smaller depth.
		if(xDepth < yDepth) {
			if(dist.x > 0) {
				setPosition(Sprite::getPosition().x + xDepth, Sprite::getPosition().y);
			}
			else {
				setPosition(Sprite::getPosition().x - xDepth, Sprite::getPosition().y);
			}
		}
		else {
			if(dist.y > 0) {
				setPosition(Sprite::getPosition().x, Sprite::getPosition().y + yDepth);
			}
			else {
				setPosition(Sprite::getPosition().x, Sprite::getPosition().y - yDepth);
			}
		}
	}
}

void Player::handleInput() {
	if(_inputManager->isKeyDown(SDLK_w)) {
		setPosition(Sprite::getPosition().x, Sprite::getPosition().y + SPEED);
	}
	if(_inputManager->isKeyDown(SDLK_s)) {
		setPosition(Sprite::getPosition().x, Sprite::getPosition().y - SPEED);
	}
	if(_inputManager->isKeyDown(SDLK_a)) {
		setPosition(Sprite::getPosition().x - SPEED, Sprite::getPosition().y);
	}
	if(_inputManager->isKeyDown(SDLK_d)) {
		setPosition(Sprite::getPosition().x + SPEED, Sprite::getPosition().y);
	}
}