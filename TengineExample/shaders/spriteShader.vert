#version 150

// Used to transform from NDC to cartesian coordinates.
uniform mat4 pMatrix;

in vec2 vertexPosition;
in vec4 vertexColor;
in vec2 vertexUV;

out vec2 fragmentPosition;
out vec4 fragmentColor;
out vec2 fragmentUV;

void main() {
	gl_Position = pMatrix * vec4(vertexPosition.x, vertexPosition.y, 0.0, 1.0);
	fragmentPosition = vertexPosition;
	fragmentColor = vertexColor; // Just want to pass color through to the fragment shader where is can be manipulated.
	fragmentUV = vec2(vertexUV.x, 1.0f - vertexUV.y);	// Flip the texture so it isn't rendered upside down.
}