#version 150

uniform sampler2D textureSampler;
uniform vec2 lightPosition;
uniform float time;

in vec2 fragmentPosition;
in vec4 fragmentColor;
in vec2 fragmentUV;

out vec4 outColor;

void main() {
	float intensity = 1.0 / length(fragmentPosition - lightPosition) * 50;

	vec4 textureColor = texture(textureSampler, fragmentUV);
	outColor = vec4(textureColor.x, textureColor.y, textureColor.z, textureColor.w) * intensity;
}