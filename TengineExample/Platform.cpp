#include "Platform.h"

Platform::Platform(float x, float y, const std::string& texturePath) : originalY(Sprite::getPosition().y), speed(1.0f), Sprite(x, y, texturePath), Collidable(x, y, Sprite::getWidth(), Sprite::getHeight()) { }

Platform::~Platform() { }

void Platform::update() {
	if(Sprite::getPosition().y == originalY + 100) {
		speed = -1.0f;
	}
	else if(Sprite::getPosition().y == originalY) {
		speed = 1.0f;
	}
	setPosition(Sprite::getPosition().x, Sprite::getPosition().y + speed);
}