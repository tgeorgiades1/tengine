#include "Tengine.h"

namespace tengine {
	int init() {
		// Initialize SDL.
		SDL_Init(SDL_INIT_EVERYTHING);

		// Set up double buffering.
		SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

		return 0;
	}

	int cleanUp() {
		// Quit SDL
		SDL_Quit();

		return 0;
	}
}