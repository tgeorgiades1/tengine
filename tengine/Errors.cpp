#include <SDL/SDL.h>
#include <iostream>
#include "Errors.h"

namespace tengine {
	void fatalError(std::string errorString) {
		std::cout << errorString + "\nEnter any key to quit...\n";
		char tmp;
		std::cin >> tmp;

		SDL_Quit();
		exit(1);
	}

	void fatalSDLError(std::string errorString) {
		const char* SDLError = SDL_GetError();
		std::cout << errorString + "\n" + SDLError + "\nEnter any key to quit...\n";
		char tmp;
		std::cin >> tmp;

		SDL_Quit();
		exit(1);
	}
}