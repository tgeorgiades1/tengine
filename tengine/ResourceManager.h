#pragma once
#include <map>
#include <string>
#include "GLTexture.h"

namespace tengine {
	class ResourceManager {
	public:
		ResourceManager();
		~ResourceManager();

		static GLTexture getTexture(const std::string& texturePath);

	private:
		static std::map<std::string, GLTexture> _textureMap;
	};
}
