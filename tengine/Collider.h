#pragma once
#include <glm/glm.hpp>

namespace tengine {
	struct AABB {
		glm::vec2 position;
		float width, height;

		AABB() : position(), width(), height() { };
		AABB(const glm::vec2& Position, float Width, float Height) : position(Position), width(Width), height(Height) { };
	};

	class Collider {
	public:
		Collider();
		Collider(float x, float y, float width, float height);
		~Collider();

		bool checkCollision(Collider* c);

		void setPosition(glm::vec2 newPosition) { _box->position.x = newPosition.x; _box->position.y = newPosition.y; }
		void setDimensions(glm::vec2 dimensions) { _box->width = dimensions.x; _box->height = dimensions.y; }

		glm::vec2 getPosition() { return _box->position; }
		glm::vec2 getCenter() { return glm::vec2(_box->position.x + _box->width / 2, _box->position.y + _box->height / 2); }

		float getWidth() { return _box->width; }
		float getHeight() { return _box->height; }

	private:
		AABB* _box;
	};
}
