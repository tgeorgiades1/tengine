#include "Timing.h"
#include <SDL/SDL.h>

namespace tengine {
	FPSTimer::FPSTimer() { }
	FPSTimer::~FPSTimer() { }

	void FPSTimer::start() {
		_startTicks = SDL_GetTicks();
	}

	float FPSTimer::end() {
		calculateFPS();

		// The amount of time it took to calculate the FPS.
		float calculationDuration = SDL_GetTicks() - _startTicks;

		// If our calculation took less time than our FPS we need to delay to fill out the time.
		if(1000 / _maxFPS > calculationDuration) {
			// The amount of time we need to delay is our FPS (in milliseconds) - the amount of time it took to calculate the FPS.
			SDL_Delay(1000.0 / _maxFPS - calculationDuration);
		}

		return _currentFPS;
	}

	void FPSTimer::calculateFPS() {
		// Number of samples we want to average to get FPS.
		static const int NUM_SAMPLES = 10;
		// Static so will store values from previous calls.
		static float fpsSamples[NUM_SAMPLES];
		//The current position in the frameTimes array.
		static int currentSample = 0;

		static float prevTicks = SDL_GetTicks();

		float currentTicks;
		currentTicks = SDL_GetTicks();

		fpsSamples[currentSample % NUM_SAMPLES] = currentTicks - prevTicks;

		prevTicks = currentTicks;

		// Keeps track of the amount of samples we have in our array so we can adjust the for loop accordingly.
		int count;
		currentSample++;
		if(currentSample < NUM_SAMPLES) {
			count = currentSample;
		}
		else {
			count = NUM_SAMPLES;
		}

		// Calculate the average of all samples.
		float sampleAverage = 0;
		for(int i = 0; i < count; i++) {
			sampleAverage += fpsSamples[i];
		}
		sampleAverage /= count;

		_currentFPS = 1000.0f / sampleAverage;
	}
}