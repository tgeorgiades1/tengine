#pragma once
#include <string>
#include <vector>
#include "GLTexture.h"

namespace tengine {
	class IOManager {
	public:
		/// Returns a string containing the exact contents of a file from the specified file path.
		static std::string getFileContents(std::string filePath);
		static bool readFileToBuffer(std::string filePath, std::vector<unsigned char>& buffer);
		static GLTexture loadPNG(std::string filePath);
	};
}
