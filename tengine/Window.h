#pragma once
#include <SDL/SDL.h>
#include <string>

namespace tengine {
	class Window {
	public:
		Window(void);
		~Window(void);

		void create(char* name, int windowWidth, int windowHeight, Uint32 flags = 0);
		void destroy();

		/// Swaps the back and front buffers for drawing.
		void swapBuffer();

		int getWindowWidth() { return _windowWidth; }
		int getWindowHeight() { return _windowHeight; }

	private:
		SDL_Window* _sdlWindow;
		SDL_GLContext _glContext;
		int _windowWidth, _windowHeight;
	};
}
