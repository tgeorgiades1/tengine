#pragma once
#include <GL/glew.h>

namespace tengine {
	struct GLTexture {
		GLuint textureID;
		int width;
		int height;
	};
}