#include "InputManager.h"

namespace tengine {
	InputManager::InputManager() { }

	InputManager::~InputManager() { }

	void InputManager::update() {
		for(auto& it : _keyMap) {
			_pKeyMap[it.first] = it.second;
		}
	}

	void InputManager::keyPressed(unsigned int keyID) {
		_keyMap[keyID] = true;
	}

	void InputManager::keyReleased(unsigned int keyID) {
		_keyMap[keyID] = false;
	}

	void InputManager::setMousePosition(float x, float y) {
		_mouseCoords.x = x;
		_mouseCoords.y = y;
	}

	bool InputManager::isKeyPressed(unsigned int keyID) {
		if(isKeyDown(keyID) && !_pKeyMap[keyID]) {
			return true;
		}
		return false;
	}

	bool InputManager::isKeyDown(unsigned int keyID) {
		auto it = _keyMap.find(keyID);
		if(it != _keyMap.end()) {
			return _keyMap[keyID];
		}
		return false;
	}
}