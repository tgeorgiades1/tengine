#pragma once
#include <string>
#include <GL/glew.h>
#include <glm/glm.hpp>

namespace tengine {
	class ShaderProgram {
	public:
		ShaderProgram();
		~ShaderProgram();

		/// Compiles a vertex and fragment shader.
		void compilePrograms(const std::string& vertexShaderPath, const std::string& fragmentShaderPath);
		/// Links the compiled vertex and fragment shaders together.
		void linkShaders();

		/// Tells openGL to use the shader program.
		void use();
		/// Tells openGL to unuse the shader program.
		void unuse();

		/// Returns the location of the specified uniform within the program.
		GLint getUniformLocation(std::string uniformName);

		/// Returns true if the program is currently in use.
		bool isInUse() { return _inUse; };

		GLuint getProgramID() { return _programID; };

		void uniform1i(char* uniformName, int val);
		void uniform1f(char* uniformName, float val);
		void uniform2f(char* uniformName, float val, float val2);
		void uniform3f(char* uniformName, float val, float val2, float val3);
		void uniform4f(char* uniformName, float val, float val2, float val3, float val4);
		void uniformMat4f(char* uniformName, glm::mat4& data);

	private:
		/// IDs corresponding to the whole program as well as its components.
		GLuint _programID, _vertexShaderID, _fragmentShaderID;
		/// The number of attributes within the program.
		int _numAttributes;

		bool _inUse;

		/// Does the actual compilation of either a vertex or fragment shader.
		void compileProgram(GLuint programID, const std::string& shaderFilePath);

		/// Check for errors in compilation or linking.
		/// Type denotes which option to check against.
		bool checkErrors(GLuint programID, GLenum type);
	};
}
