#include "IOManager.h"
#include "picoPNG.h"
#include "Errors.h"
#include <fstream>
#include <GL/glew.h>

namespace tengine {
	std::string IOManager::getFileContents(std::string filePath) {
		std::fstream file;
		file.open(filePath.c_str());
		std::string fileContents;
		std::string line;
		if(file) {
			while(std::getline(file, line)) {
				fileContents += line + "\n";
			}

			file.close();
		}

		return fileContents;
	}

	bool IOManager::readFileToBuffer(std::string filePath, std::vector<unsigned char>& buffer) {
		std::ifstream file(filePath, std::ios::binary);
		if(file.fail()) {
			perror(filePath.c_str());
			return false;
		}

		// Get length of file
		file.seekg(0, std::ios::end);
		int size = file.tellg();
		file.seekg(0, std::ios::beg);
		// Remove any header bytes if any.
		//size -= file.tellg();

		buffer.resize(size);

		file.read((char*)&(buffer[0]), size);
		file.close();

		return true;
	}

	GLTexture IOManager::loadPNG(std::string filePath) {
		// Load an get the image information.
		std::vector<unsigned char> output;
		std::vector<unsigned char> input;	// Input to picoPNG
		unsigned long width, height;

		if(!IOManager::readFileToBuffer(filePath, input)) {
			fatalError("Error reading file: " + filePath + "\n");
		}

		// Get the image information.
		int error = decodePNG(output, width, height, &(input[0]), input.size());

		if(error != 0) {
			fatalError("Error decoding file: " + filePath + "! Code #" + std::to_string(error) + "\n");
		}

		// Create and set up the texture.
		GLTexture tex = {};	// Inits everything in struct to 0.
		glGenTextures(1, &(tex.textureID));

		glBindTexture(GL_TEXTURE_2D, tex.textureID);

		// Upload the image data.
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, &(output[0]));

		// Set the texture to repeat if it has to wrap in either direction.
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// (x, y, z) == (s, t, r)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

		// Choose how the image is filtered (rendered when scaled up or down).
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		// Generate Mipmaps (smaller copies of the texture that have been scaled down and filtered in advance).
		glGenerateMipmap(GL_TEXTURE_2D);

		// Unbind the texture.
		glBindTexture(GL_TEXTURE_2D, 0);

		tex.width = width;
		tex.height = height;

		return tex;
	}
}