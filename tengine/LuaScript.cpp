#include "LuaScript.h"

namespace tengine {
	LuaScript::LuaScript(const std::string& scriptPath) {
		L = lua_open();
		luaL_openlibs(L);
		if(luaL_loadfile(L, scriptPath.c_str()) || lua_pcall(L, 0, 0, 0)) {
			fatalError("Failed to open script: " + scriptPath);
			exit(0);
		}
	}

	LuaScript::~LuaScript() {
		if(L) lua_close(L);
	}

	void LuaScript::callFunction(const std::string& funcName) {
		// Push the function to the top of the stack.
		lua_getglobal(L, funcName.c_str());

		// Call the function.
		int callError = 0;
		if(callError = lua_pcall(L, 0, 0, 0) != 0) {
			fatalError("The function " + funcName + " failed: " + lua_tostring(L, -1));
		}
	}

	void LuaScript::splitDataPath(std::vector<std::string>* buffer, const std::string& dataPath, char delimiter) {
		int prevPos = 0;
		int pos = 0;
		while(pos != std::string::npos) {
			pos = dataPath.find(delimiter, prevPos);
			buffer->push_back(dataPath.substr(prevPos, pos - prevPos));	 // Need to start at prevPos in the string and take a string of len pos - prevPos
			prevPos = pos + 1;
		}
	}
}