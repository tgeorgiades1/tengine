#include "Collider.h"

namespace tengine {
	Collider::Collider() {
		_box = new AABB();
	}

	Collider::Collider(float x, float y, float width, float height) {
		_box = new AABB(glm::vec2(x, y), width, height);
	}

	Collider::~Collider() {
		delete _box;
	}

	bool Collider::checkCollision(Collider* collidee) {
		if((_box->position.x + _box->width >= collidee->_box->position.x) &&
			(_box->position.x <= collidee->_box->position.x + collidee->_box->width) &&
			(_box->position.y + _box->height >= collidee->_box->position.y) &&
			(_box->position.y <= collidee->_box->position.y + collidee->_box->height)) {
			return true;
		}
		return false;
	}
}