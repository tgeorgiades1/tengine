#include "Camera2D.h"

namespace tengine {
	Camera2D::Camera2D(int screenWidth, int screenHeight) :
		_worldMatrix(1.0f),
		_orthoMatrix(1.0f),
		_needsUpdate(true),
		_screenWidth(screenWidth),
		_screenHeight(screenHeight),
		_position(0.0f, 0.0f),
		_scalingFactor(1.0f) {
		_orthoMatrix = glm::ortho(0.0f, (float)_screenWidth, 0.0f, (float)_screenHeight);
	}

	Camera2D::~Camera2D() { }

	void Camera2D::update() {
		if(_needsUpdate) {
			glm::vec3 translation(-_position.x + _screenWidth / 2, -_position.y + _screenHeight / 2, 0.0f);
			_worldMatrix = glm::translate(_orthoMatrix, translation);

			glm::vec3 scale(_scalingFactor, _scalingFactor, 0.0f);
			_worldMatrix = glm::scale(glm::mat4(1.0f), scale) * _worldMatrix;

			_needsUpdate = false;
		}
	}

	glm::vec2 Camera2D::convertScreenToWorld(glm::vec2 screenCoords) {
		//Invert y direction
		screenCoords.y = _screenHeight - screenCoords.y;

		screenCoords -= glm::vec2(_screenWidth / 2, _screenHeight / 2);

		screenCoords /= _scalingFactor;

		screenCoords += _position;

		return screenCoords;
	}
}