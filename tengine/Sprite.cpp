#include "Sprite.h"

namespace tengine {
	Sprite::Sprite() { }

	Sprite::Sprite(float x, float y, const std::string& texturePath) :
		_position(x, y),
		_width(0),
		_height(0) {
		_color.a = 255;
		_color.r = 255;
		_color.b = 255;
		_color.g = 255;

		_texture = ResourceManager::getTexture(texturePath);
		_width = _texture.width;
		_height = _texture.height;
	}

	Sprite::~Sprite() { }

	void Sprite::draw(tengine::SpriteBatch& spriteBatch) {
		glm::vec4 destRect;
		destRect.x = _position.x;
		destRect.y = _position.y;
		destRect.w = _width;
		destRect.z = _height;

		const glm::vec4 uvRect(0.0f, 0.0f, 1.0f, 1.0f);

		spriteBatch.draw(destRect, uvRect, _texture.textureID, 0.0f, _color, _shaderProg);
	}
}