#pragma once
extern "C" {
	#include <Lua/lua.h>
	#include <Lua/lauxlib.h>
	#include <Lua/lualib.h>
}
#include <string>
#include <vector>
#include "Errors.h"

namespace tengine {
	class LuaScript {
	public:
		LuaScript(const std::string& scriptPath);
		~LuaScript();

		template<typename T>
		T get(const std::string& dataPath) {
			std::vector<std::string> buffer;
			splitDataPath(&buffer, dataPath, '.');

			// Get the top table.
			lua_getglobal(L, buffer[0].c_str());

			if(!lua_istable(L, -1)) {
				fatalError(dataPath + " is not a valid table.");
			}

			// Traverse the table.
			for(int i = 1; i < buffer.size(); i++) {
				lua_pushstring(L, buffer[i].c_str());	// Push on a key.
				lua_gettable(L, -2);					// Push on the field corresponding to the key on top of the stack. [This pops the key]
			}

			T result = luaGet<T>();						// Get the value.

			int size = buffer.size();
			lua_pop(L, size);							// Pop all excess values on the stack to keep it balanced.

			return result;
		}

		template<typename T>
		void set(const std::string& dataPath, T value) {
			std::vector<std::string> buffer;
			splitDataPath(&buffer, dataPath, '.');

			lua_getglobal(L, buffer[0].c_str());

			if(!lua_istable(L, -1)) {
				fatalError(dataPath + " is not a valid table.");
			}

			// Traverse the table.
			for(int i = 1; i < buffer.size(); i++) {
				lua_pushstring(L, buffer[i].c_str());	// Push on a key.
				lua_gettable(L, -2);					// Push on the field corresponding to the key on top of the stack. [This pops the key]
			}

			// Push the data onto the stack.
			luaSet<T>(value);							// Push the value that is to be set onto the stack.

			int size = buffer.size();
			lua_setfield(L, -3, buffer[size - 1].c_str());	// [setfield pops the value from the stack]

			lua_pop(L, size);								// Pop all excess values on the stack to keep it balanced.
		}

		/// Call a function defined in lua.
		void callFunction(const std::string& funcName);

	private:
		lua_State* L;

		/// Splits the data path for any value within a table.
		/// Data paths should be table names separated by a '.' the last item should be the key for some value. Ex: player.position.x 
		void splitDataPath(std::vector<std::string>* buffer, const std::string& dataPath, char delimiter);

		/// Pushes data based upon type to the lua stack.
		template<typename T>
		void luaSet(T data);

		template <>
		inline void LuaScript::luaSet(bool data) {
			lua_pushboolean(L, data);
		}

		template <>
		inline void LuaScript::luaSet(int data) {
			lua_pushinteger(L, data);
		}

		template <>
		inline void LuaScript::luaSet(float data) {
			lua_pushnumber(L, data);
		}

		template <>
		inline void LuaScript::luaSet(const std::string& data) {
			lua_pushlstring(L, data.c_str(), data.size());
		}

		/// Casts the value to the correct type and gets it from the lua stack.
		template<typename T>
		T luaGet();

		/// Template get funcs for each type.
		template <>
		inline bool LuaScript::luaGet() {
			return (bool)lua_toboolean(L, -1);
		}

		template <>
		inline float LuaScript::luaGet() {
			return (float)lua_tonumber(L, -1);
		}

		template <>
		inline int LuaScript::luaGet() {
			return (int)lua_tonumber(L, -1);
		}

		template <>
		inline std::string LuaScript::luaGet() {
			std::string s = "null";
			if(lua_isstring(L, -1)) {
				s = std::string(lua_tostring(L, -1));
			}
			return s;
		}

		/// Returns the default value if some value could not be found in the script.
		template<typename T>
		T getDefault();

		/// Returns the default string if a value could not be found in the script.
		template<>
		inline std::string LuaScript::getDefault() {
			return "null";
		}

		/// Returns the default number if a value could not be found in the script.
		template<>
		inline int LuaScript::getDefault() {
			return 0;
		}

		template<>
		inline float LuaScript::getDefault() {
			return 0.0f;
		}
	};
}
