#include "HUDButton.h"
#include <SDL/SDL.h>
#include <stdio.h>

namespace tengine {
	HUDButton::HUDButton(float x, float y, const std::string& texturePath, tengine::InputManager& inputManager, const std::string& scriptPath) :
		HUDElement(x, y, texturePath),
		_inputManager(inputManager) {
		_script = new LuaScript(scriptPath);
	}

	HUDButton::~HUDButton() {
		delete _script;
	}

	void HUDButton::update() {
		if(getPosition().x <= _inputManager.getMousePosition().x &&
			getPosition().x + getWidth() >= _inputManager.getMousePosition().x &&
			getPosition().y < _inputManager.getMousePosition().y &&
			getPosition().y + getHeight() >= _inputManager.getMousePosition().y) {
			if(_inputManager.isKeyPressed(SDL_BUTTON_LEFT)) {
				_script->set<int>("button.s", 20);
				_script->callFunction("hi");
			}
		}
	}
}