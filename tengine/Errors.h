#pragma once
#include <string>

namespace tengine {
	/// Prints out an error string and quits the game.
	void fatalError(std::string errorString);

	/// Prints out an error string including the output from SDL_GetError and quits the game.
	void fatalSDLError(std::string errorString);
}