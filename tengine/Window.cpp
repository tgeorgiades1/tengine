#include <GL/glew.h>
#include <cstdio>
#include "Window.h"
#include "Errors.h"

namespace tengine {
	Window::Window(void) { }

	Window::~Window(void) { }

	void Window::create(char* name, int _windowWidth, int _windowHeight, Uint32 flags) {
		Uint32 currentFlags = SDL_WINDOW_OPENGL;

		_sdlWindow = SDL_CreateWindow(name, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, _windowWidth, _windowHeight, currentFlags);
		if(_sdlWindow == nullptr) {
			fatalSDLError("SDL Window could not be created.");
		}

		// Create an openGL context for the window.
		_glContext = SDL_GL_CreateContext(_sdlWindow);
		if(_glContext == nullptr) {
			fatalSDLError("SDL_GL context could not be created.");
		}

		// Initialize glew
		GLenum error = glewInit();
		if(error != GLEW_OK) {
			fatalError("Glew failed to initialize. Error code: " + error);
		}

		// Print openGL version
		std::printf("OpenGL version: %s \n", glGetString(GL_VERSION));

		// Set the background color to blue.
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

		// Turn off VSYNC.
		SDL_GL_SetSwapInterval(0);

		// Enable alpha blending for transparency.
		glEnable(GL_BLEND);

		// Tell openGL how to do the blending.
		// SourceFactor - How the alpha channel affects the source (sprite). Will multiply the source by the alpha. Ex: if alpha is 0 - multiply color by 0 thus no color (transparent).
		// DestinationFactor - How alpha channel affects destination (background). Will multiply the destination Ex: if alpha is 1 (solid) 1-0=1 so multiply 1 * rgb. If alpha is 0 (transparent) 0-0=0 so multiply 0 * rgb.
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	}

	void Window::destroy() {
		SDL_GL_DeleteContext(_glContext);
	}

	void Window::swapBuffer() {
		SDL_GL_SwapWindow(_sdlWindow);
	}
}