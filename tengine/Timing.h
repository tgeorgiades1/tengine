namespace tengine {
	class FPSTimer {
	public:
		FPSTimer();
		~FPSTimer();

		void setMaxFPS(float maxFPS) { _maxFPS = maxFPS; };

		void start();
		float end();

	private:
		float _startTicks;
		float _maxFPS;
		float _currentFPS;

		void calculateFPS();
	};
}