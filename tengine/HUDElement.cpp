#include "HUDElement.h"

namespace tengine {
	HUDElement::HUDElement(float x, float y, const std::string& texturePath) : Sprite(x, y, texturePath) { }

	HUDElement::~HUDElement() { }
}