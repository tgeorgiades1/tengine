#pragma once
#include "Sprite.h"
#include "InputManager.h"
#include "HUDElement.h"
#include "LuaScript.h"

namespace tengine {
	/// A button that will do something when clicked.
	class HUDButton : public HUDElement {
	public:
		/// Takes in a function pointer that links to the function this button should call when pressed.
		HUDButton(float x, float y, const std::string& texturePath, tengine::InputManager& inputManager, const std::string& scriptPath);
		~HUDButton();

		void update();

	private:
		InputManager& _inputManager;
		LuaScript* _script;
	};
}
