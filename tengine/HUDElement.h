#pragma once
#include "Sprite.h"

namespace tengine {
	/// Base class for all elements belonging to the HUD.
	class HUDElement : public Sprite {
	public:
		HUDElement(float x, float y, const std::string& texturePath);
		~HUDElement();

		virtual void update() = 0;
	};
}
