#include "HUDContainer.h"
#include "HUDButton.h"

namespace tengine {
	HUDContainer::HUDContainer(int screenWidth, int screenHeight) :
		_time(0.0f),
		_screenWidth(screenWidth),
		_screenHeight(screenHeight) {
		_guiProg = new ShaderProgram();
		_guiProg->compilePrograms(BASE_GUI_VERT_SHADER, BASE_GUI_FRAG_SHADER);
		_guiProg->linkShaders();

		_HUDMatrix = glm::ortho(0.0f, (float)screenWidth, (float)screenHeight, 0.0f);
	}

	HUDContainer::~HUDContainer() {
		for(int i = 0; i < _elements.size(); i++) {
			delete(_elements[i]);
		}
	}

	void HUDContainer::update() {
		for(auto& element : _elements) {
			element->update();
		}

		_time += 0.1f;

		_guiProg->uniform1i("textureSampler", 0);
		_guiProg->uniformMat4f("pMatrix", _HUDMatrix);
		_guiProg->uniform1f("time", _time);
	}

	void HUDContainer::draw(SpriteBatch& batch) {
		for(auto& element : _elements) {
			element->draw(batch);
		}
	}

	void HUDContainer::createButton(float x, float y, const std::string& texturePath, tengine::InputManager& inputManager, const std::string& scriptPath) {
		HUDButton* button = new HUDButton(x, y, texturePath, inputManager, scriptPath);
		// Need to adjust the y coordinate because the y axis of OpenGL grows from top to bottom.
		button->setPosition(x, _screenHeight - button->getHeight() - y);
		button->attachShader(_guiProg);
		_elements.push_back(button);
	}
}