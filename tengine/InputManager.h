#pragma once
#include <glm/glm.hpp>
#include <unordered_map>

namespace tengine {
	class InputManager {
	public:
		InputManager();
		~InputManager();

		/// Loops through key map and copies it over to the previous key map.
		void update();

		/// Updates the key map to reflect that a key has been pressed.
		/// A pressed key is not held. It will only cause one output of true.
		void keyPressed(unsigned int keyID);

		/// Updates the key map to reflect that a key has been released.
		void keyReleased(unsigned int keyID);

		/// Sets the mouse coordinates to the specified values.
		void setMousePosition(float x, float y);

		/// Returns the mouse position.
		glm::vec2 getMousePosition() { return _mouseCoords; };

		/// Returns whether the specified key is pressed.
		/// A pressed key is not held. It will only cause one output of true.
		bool isKeyPressed(unsigned int keyID);

		/// Returns whether the specified key is down.
		/// A down key is held. It will output true as long as the key is held down.
		bool isKeyDown(unsigned int keyID);

	private:
		/// Stores the current mapping of keys to their state.
		std::unordered_map<unsigned int, bool> _keyMap;
		/// Stores the previous mapping of keys to their state.
		std::unordered_map<unsigned int, bool> _pKeyMap;

		/// Stores the current mouse coordinates.
		glm::vec2 _mouseCoords;
	};
}
