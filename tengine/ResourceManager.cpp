#include "ResourceManager.h"
#include "IOManager.h"

namespace tengine {
	std::map<std::string, GLTexture> ResourceManager::_textureMap;	// Forward declaration.

	ResourceManager::ResourceManager() { }

	ResourceManager::~ResourceManager() { }

	GLTexture ResourceManager::getTexture(const std::string& texturePath) {
		auto mapIt = _textureMap.find(texturePath);

		if(mapIt == _textureMap.end()) {
			GLTexture newTexture = IOManager::loadPNG(texturePath);
			_textureMap.insert(std::make_pair(texturePath, newTexture));

			return newTexture;
		}

		return mapIt->second;
	}
}