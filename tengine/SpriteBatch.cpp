#include "SpriteBatch.h"
#include <algorithm>

namespace tengine {
	SpriteBatch::SpriteBatch() : _vboID(0), _vaoID(0) { }

	SpriteBatch::~SpriteBatch() { }

	void SpriteBatch::init() {
		initBuffers();
	}

	void SpriteBatch::begin(SortType sortType) {
		// Deallocate all memory and clear both vectors to set up for the next draw.
		_sortType = sortType;

		_batches.clear();
		for(int i = 0; i < _spriteData.size(); i++) {
			delete _spriteData[i];
		}
		_spriteData.clear();
	}

	void SpriteBatch::draw(const glm::vec4& destinationRect, const glm::vec4& uvRect, GLuint textureID, float depth, const Color& color, ShaderProgram* shaderProg) {
		SpriteData* newData = new SpriteData();

		newData->bottomLeft.setPosition(destinationRect.x, destinationRect.y);
		newData->bottomLeft.setUV(uvRect.x, uvRect.y);
		newData->bottomLeft.color = color;

		newData->bottomRight.setPosition(destinationRect.x + destinationRect.w, destinationRect.y);
		newData->bottomRight.setUV(uvRect.x + uvRect.z, uvRect.y);
		newData->bottomRight.color = color;

		newData->topRight.setPosition(destinationRect.x + destinationRect.w, destinationRect.y + destinationRect.z);
		newData->topRight.setUV(uvRect.x + uvRect.z, uvRect.y + uvRect.w);
		newData->topRight.color = color;

		newData->topLeft.setPosition(destinationRect.x, destinationRect.y + destinationRect.z);
		newData->topLeft.setUV(uvRect.x, uvRect.y + uvRect.w);
		newData->topLeft.color = color;

		newData->depth = depth;
		newData->textureID = textureID;
		newData->shaderProg = shaderProg;

		_spriteData.push_back(newData);
	}

	void SpriteBatch::end() {
		sortSpriteData();
		createBatches();
		renderBatches();
	}

	void SpriteBatch::initBuffers() {
		if(_vaoID == 0) {
			glGenVertexArrays(1, &_vaoID);
		}
		glBindVertexArray(_vaoID);

		if(_vboID == 0) {
			glGenBuffers(1, &_vboID);
		}
		glBindBuffer(GL_ARRAY_BUFFER, _vboID);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);

		// Create references to where the different data is stored in the vbo.
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, position));
		glVertexAttribPointer(1, 4, GL_UNSIGNED_BYTE, GL_TRUE, sizeof(GLVertex), (void*)offsetof(GLVertex, color));
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(GLVertex), (void*)offsetof(GLVertex, uv));

		// Unbind the vao.
		glBindVertexArray(0);
	}

	void SpriteBatch::createBatches() {
		std::vector<GLVertex> vertices;

		// Resize the vector to accomodate all the incoming vertices. Also allows us to use the vector as if it were an array.
		vertices.resize(_spriteData.size() * 6);

		if(_spriteData.empty()) {
			return;
		}

		// Create the first batch.
		int offset = 0;
		int currentVertex = 0;

		_batches.emplace_back(_spriteData[0]->textureID, offset, 6, _spriteData[0]->shaderProg);

		// Triangle one.
		vertices[currentVertex++] = _spriteData[0]->topLeft;
		vertices[currentVertex++] = _spriteData[0]->bottomLeft;
		vertices[currentVertex++] = _spriteData[0]->bottomRight;
		// Triangle two.
		vertices[currentVertex++] = _spriteData[0]->bottomRight;
		vertices[currentVertex++] = _spriteData[0]->topRight;
		vertices[currentVertex++] = _spriteData[0]->topLeft;
		offset += 6;

		for(int i = 1; i < _spriteData.size(); i++) {
			// Check if the texture of the previous sprite is the same as the current one. If the same: do not create a new batch.
			if(_spriteData[i]->textureID != _spriteData[i - 1]->textureID) {
				_batches.emplace_back(_spriteData[i]->textureID, offset, 6, _spriteData[i]->shaderProg);
			}
			else {
				// Add 6 more vertices to the count for the current batch.
				_batches.back().numVertices += 6;
			}
			// Add the vertex data to the vertices vector.
			vertices[currentVertex++] = _spriteData[i]->topLeft;
			vertices[currentVertex++] = _spriteData[i]->bottomLeft;
			vertices[currentVertex++] = _spriteData[i]->bottomRight;

			vertices[currentVertex++] = _spriteData[i]->bottomRight;
			vertices[currentVertex++] = _spriteData[i]->topRight;
			vertices[currentVertex++] = _spriteData[i]->topLeft;
			offset += 6;
		}

		glBindBuffer(GL_ARRAY_BUFFER, _vboID);

		// Empty the buffer of all data.
		glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(GLVertex), nullptr, GL_DYNAMIC_DRAW);
		// Add all vertex data to the buffer.
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(GLVertex), vertices.data());
		// Unbind the buffer.
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	void SpriteBatch::renderBatches() {
		glBindVertexArray(_vaoID);

		for(int i = 0; i < _batches.size(); i++) {
			_batches[i].shaderProg->use();
			glBindTexture(GL_TEXTURE_2D, _batches[i].textureID);
			glDrawArrays(GL_TRIANGLES, _batches[i].offset, _batches[i].numVertices);
		}

		glBindVertexArray(0);
	}

	//Sort based upon what our sort type is.
	void SpriteBatch::sortSpriteData() {
		switch(_sortType) {
		case SortType::BACK_TO_FRONT:
			std::stable_sort(_spriteData.begin(), _spriteData.end(), compareBackToFront);
			break;
		case SortType::FRONT_TO_BACK:
			std::stable_sort(_spriteData.begin(), _spriteData.end(), compareFrontToBack);
			break;
		case SortType::TEXTURE:
			std::stable_sort(_spriteData.begin(), _spriteData.end(), compareTexture);
			break;
		case SortType::SHADER:
			std::stable_sort(_spriteData.begin(), _spriteData.end(), compareShader);
			break;
		}
	}

	// For stable sort.
	bool SpriteBatch::compareFrontToBack(SpriteData* a, SpriteData* b) {
		return (a->depth < b->depth);
	}
	bool SpriteBatch::compareBackToFront(SpriteData* a, SpriteData* b) {
		return (a->depth > b->depth);
	}
	bool SpriteBatch::compareTexture(SpriteData* a, SpriteData* b) {
		return (a->textureID < b->textureID);
	}
	bool SpriteBatch::compareShader(SpriteData* a, SpriteData* b) {
		return (a->shaderProg->getProgramID() < b->shaderProg->getProgramID());
	}
}