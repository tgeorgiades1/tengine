#pragma once
#include <SDL/SDL.h>

namespace tengine {
	/// Initializes SDL and sets up double buffering.
	int init();
	/// Quits SDL.
	int cleanUp();
}