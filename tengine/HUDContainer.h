#pragma once
#include "HUDElement.h"
#include "InputManager.h"
#include "Camera2D.h"

#include <vector>
#include <glm/glm.hpp>

namespace tengine {
	/// Contains all HUDElements that made up the players HUD.
	class HUDContainer {
	public:
		HUDContainer(int screenWidth, int screenHeight);
		~HUDContainer();

		/// Updates all HUDElements.
		void update();

		/// Draws all HUDElements.
		void draw(SpriteBatch& batch);

		/// Instantiates a button and places it in the vector.
		void createButton(float x, float y, const std::string& texturePath, tengine::InputManager& inputManager, const std::string& scriptPath);

	private:
		float _time;

		glm::mat4 _HUDMatrix;

		int _screenWidth, _screenHeight;

		/// Basic shaders that the GUI should use if the user does not want to create their own.
		const std::string BASE_GUI_VERT_SHADER = "../tengine/shaders/baseGUIShader.vert";
		const std::string BASE_GUI_FRAG_SHADER = "../tengine/shaders/baseGUIShader.frag";
		ShaderProgram* _guiProg;

		std::vector<HUDElement*> _elements;
	};
}
