#include "ShaderProgram.h"
#include "IOManager.h"
#include "Errors.h"

namespace tengine {
	ShaderProgram::ShaderProgram() : _programID(0), _vertexShaderID(0), _fragmentShaderID(0), _numAttributes(0), _inUse(false) { }

	ShaderProgram::~ShaderProgram() { }

	void ShaderProgram::compilePrograms(const std::string& vertexShaderPath, const std::string& fragmentShaderPath) {
		// Create the separate shader objects.
		_vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		if(_vertexShaderID == 0) {
			fatalError("Vertex shader failed to be created!");
		}
		_fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		if(_fragmentShaderID == 0) {
			fatalError("Vertex shader failed to be created!");
		}

		compileProgram(_vertexShaderID, vertexShaderPath);
		compileProgram(_fragmentShaderID, fragmentShaderPath);
	}

	void ShaderProgram::linkShaders() {
		// Create the shader object that will hold the combined the vert and frag shaders.
		_programID = glCreateProgram();

		// Attach the vert and frag shaders to the program.
		glAttachShader(_programID, _vertexShaderID);
		glAttachShader(_programID, _fragmentShaderID);

		// Link the program.
		glLinkProgram(_programID);

		// Check for linker errors.
		checkErrors(_programID, GL_LINK_STATUS);

		// Detach then delete the shaders objects as they are not needed anymore.
		glDetachShader(_programID, _vertexShaderID);
		glDetachShader(_programID, _fragmentShaderID);
		glDeleteShader(_vertexShaderID);
		glDeleteShader(_fragmentShaderID);
	}

	void ShaderProgram::use() {
		glUseProgram(_programID);
		_inUse = true;
	}

	void ShaderProgram::unuse() {
		glUseProgram(0);
		_inUse = false;
	}

	// TODO: Cache names. glGetUniformLoc is slow.
	GLint ShaderProgram::getUniformLocation(std::string uniformName) {
		GLint uniformLoc = glGetUniformLocation(_programID, uniformName.c_str());

		if(uniformLoc == GL_INVALID_INDEX) {
			fatalError("The uniform: " + uniformName + " was not found in shader!");
		}

		return uniformLoc;
	}

	void ShaderProgram::uniform1i(char* uniformName, int val) {
		glUniform1iv(getUniformLocation(uniformName), 1, &val);
	}

	void ShaderProgram::uniform1f(char* uniformName, float val) {
		glUniform1fv(getUniformLocation(uniformName), 1, &val);
	}

	void ShaderProgram::uniform2f(char* uniformName, float val, float val2) {
		float data[] = { val, val2 };
		glUniform2fv(getUniformLocation(uniformName), 1, data);
	}

	void ShaderProgram::uniform3f(char* uniformName, float val, float val2, float val3) {
		float data[] = { val, val2, val3 };
		glUniform3fv(getUniformLocation(uniformName), 1, data);
	}

	void ShaderProgram::uniform4f(char* uniformName, float val, float val2, float val3, float val4) {
		float data[] = { val, val2, val3, val4 };
		glUniform4fv(getUniformLocation(uniformName), 1, data);
	}

	void ShaderProgram::uniformMat4f(char* uniformName, glm::mat4& data) {
		glUniformMatrix4fv(getUniformLocation(uniformName), 1, GL_FALSE, &data[0][0]);
	}

	void ShaderProgram::compileProgram(GLuint programID, const std::string& shaderFilePath) {
		std::string fileContents = IOManager::getFileContents(shaderFilePath);
		const char* fileContentsPtr = fileContents.c_str();

		// Load the data into the shader object.
		glShaderSource(programID, 1, &fileContentsPtr, NULL);

		glCompileShader(programID);

		checkErrors(programID, GL_COMPILE_STATUS);
	}

	bool ShaderProgram::checkErrors(GLuint programID, GLenum type) {
		GLint status;
		// Check for compilation errors.
		if(type == GL_COMPILE_STATUS) {
			glGetShaderiv(programID, GL_COMPILE_STATUS, &status);
		}
		else {	// Linker  error
			glGetProgramiv(programID, GL_LINK_STATUS, &status);
		}
		if(status != GL_TRUE){
			char buffer[512];
			glGetShaderInfoLog(programID, 512, NULL, buffer);
			fatalError(buffer);
		}
		return true;
	}
}