#pragma once
#include "GLVertex.h"
#include "GLTexture.h"
#include "SpriteBatch.h"
#include "ShaderProgram.h"
#include "ResourceManager.h"

#include <glm/glm.hpp>
#include <string>

namespace tengine {
	class Sprite {
	public:
		Sprite();
		Sprite(float x, float y, const std::string& texturePath);
		virtual ~Sprite();

		/// Draws all elements of the Sprite
		void draw(tengine::SpriteBatch& spriteBatch);

		void attachShader(ShaderProgram* program) { _shaderProg = program; };

		void setPosition(float x, float y) { _position.x = x; _position.y = y; };

		void setTexture(const std::string& texturePath) {
			_texture = ResourceManager::getTexture(texturePath);
			_width = _texture.width;
			_height = _texture.height;
		}

		glm::vec2 getPosition() { return _position; };
		glm::vec2 getCenter() { return glm::vec2(_position.x + _width / 2, _position.y + _height / 2); };
		float getWidth() { return _width; };
		float getHeight() { return _height; };

	protected:
		glm::vec2 _position;

	private:
		tengine::GLTexture _texture;

		ShaderProgram* _shaderProg;
		float _width, _height;
		Color _color;
	};
}
