#pragma once
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace tengine {
	class Camera2D {
	public:
		Camera2D(int screenWidth, int screenHeight);
		~Camera2D();

		/// Updates the camera matrix. Should be called every frame but only acts when the position or scale is changed.
		void update();

		glm::vec2 convertScreenToWorld(glm::vec2 mouseCoords);

		void setPosition(const glm::vec2& position){ _position = position; _needsUpdate = true; };
		void setScale(float scalingFactor) { _scalingFactor = scalingFactor; _needsUpdate = true; };

		glm::vec2 getPosition() { return _position; };
		float getScale() { return _scalingFactor; };
		glm::mat4 getWorldMatrix() { return _worldMatrix; };

	private:
		bool _needsUpdate;
		int _screenWidth, _screenHeight;
		float _scalingFactor;
		glm::vec2 _position;

		glm::mat4 _orthoMatrix;
		glm::mat4 _worldMatrix;
	};
}
