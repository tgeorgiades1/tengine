#pragma once
#include "ShaderProgram.h"
#include "GLVertex.h"

#include <GL/glew.h>
#include <glm./glm.hpp>
#include <vector>
#include <map>

namespace tengine {
	/// Options to sort the sprite data.
	enum class SortType {
		FRONT_TO_BACK,
		BACK_TO_FRONT,
		TEXTURE,
		SHADER
	};

	/// Contains the sprite information that will be put into batches.
	struct SpriteData {
		GLuint textureID;
		ShaderProgram* shaderProg;

		float depth;

		GLVertex bottomLeft;
		GLVertex bottomRight;
		GLVertex topRight;
		GLVertex topLeft;
	};

	/// Contains all sprite data for sprites with the same texture.
	struct Batch {
		GLuint offset;
		GLuint numVertices;
		GLuint textureID;
		ShaderProgram* shaderProg;

		Batch(GLuint textureID, GLuint offset, GLuint numVertices, ShaderProgram* shaderProg) : textureID(textureID), offset(offset), numVertices(numVertices), shaderProg(shaderProg) { };
	};

	class SpriteBatch {
	public:
		SpriteBatch();
		~SpriteBatch();

		/// Clear out all previous data and set up for the next draw.
		void begin(SortType sortType = SortType::TEXTURE);

		/// Adds the sprite data to a batch.
		/// destinationRect contains the x, y, width and height of a sprite. uvRect is similar.
		void draw(const glm::vec4& destinationRect, const glm::vec4& uvRect, GLuint textureID, float depth, const Color& color, ShaderProgram* shaderProg);

		/// Actually draw everything.
		void end();

		void init();

	private:
		GLuint _vboID;
		GLuint _vaoID;

		std::vector<SpriteData*> _spriteData;
		std::vector<Batch> _batches;

		SortType _sortType;

		void initBuffers();
		void createBatches();
		void renderBatches();
		void sortSpriteData();

		/// Predicates for stable_sort
		static bool compareFrontToBack(SpriteData* a, SpriteData* b);
		static bool compareBackToFront(SpriteData* a, SpriteData* b);
		static bool compareTexture(SpriteData* a, SpriteData* b);
		static bool compareShader(SpriteData* a, SpriteData *b);
	};
}