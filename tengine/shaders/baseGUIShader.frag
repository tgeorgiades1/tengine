#version 150

uniform sampler2D textureSampler;
uniform float time;

in vec2 fragmentPosition;
in vec4 fragmentColor;
in vec2 fragmentUV;

out vec4 outColor;

void main() {
	vec4 textureColor = texture(textureSampler, fragmentUV);
	outColor = textureColor * vec4(fragmentColor.r * (cos(time) + 2.0) * 0.7,
								   fragmentColor.g * (cos(time) + 2.0) * 0.7,
							       fragmentColor.b * (cos(time) + 2.0) * 0.7,
			                       fragmentColor.a); 
}